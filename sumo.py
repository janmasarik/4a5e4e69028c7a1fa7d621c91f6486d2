#!/usr/bin/python3

from ev3dev.ev3 import LargeMotor, ColorSensor, InfraredSensor, TouchSensor, Button, Sound
from time import sleep
from threading import Thread

m1 = LargeMotor('outA')
m2 = LargeMotor('outD')
cl = ColorSensor()
ir = InfraredSensor()
ts = TouchSensor()
btn = Button()

cl.mode = 'COL-REFLECT'
ir.mode = 'IR-PROX'

gonna_fly_now_lyrics = """
    Trying hard now
    It's so hard now
    Trying hard now
    Gettin' strong now
    Coming on, now
    Gettin' strong now
    Gonna fly now
    Flyin' high now
    Gonna fly, fly, fly
"""

def turn_around():
    """Method which is called when robot hits the edge of the playground."""
    Sound.speak('OHHH OUUUU')
    m1.run_timed(time_sp=1000, speed_sp=-1000)
    m2.run_timed(time_sp=1000, speed_sp=-1000)
    sleep(1)

    m1.run_timed(time_sp=1000, speed_sp=-800)
    m2.run_timed(time_sp=1000, speed_sp=800)
    sleep(1)

def fake_and_hit():
    """
    Method which is called when robot is hit in front.
    Robot fakes turn to get off his enemy and then hit him from the side.
    """
    m1.run_timed(time_sp=1800, speed_sp=-1000)
    sleep(1.8)
    Sound.speak('AMBUSH MOTHERF**KER!')
    m1.run_timed(time_sp=2000, speed_sp=1000)
    m2.run_timed(time_sp=2000, speed_sp=1000)
    sleep(2)


def charge():
    """Speed up the robot to highest possible speed and aggresively shout."""
    Sound.speak('DESTROY' * 10)
    m1.run_timed(time_sp=4000, speed_sp=1000)
    m2.run_timed(time_sp=4000, speed_sp=1000)
    sleep(4)

Sound.speak(gonna_fly_now_lyrics)

def check_line():
    while True:
        print('speed', cl.value())
        if cl.value() < 2:  # If line is detected, back off and turn around
            print('line_detected')
            m1.stop()
            m2.stop()
            turn_around()
        sleep(0.01)

t1 = Thread(target=check_line)
t1.start()

while True:
    m1.run_forever(speed_sp=700)
    m2.run_forever(speed_sp=700)

    if ts.value():  # on touch of the front sensor, ambush the robot
        m1.stop()
        m2.stop()
        fake_and_hit()

    if ir.value() < 30:  # charge in case robot is near of us
        m1.stop()
        m2.stop()
        charge()
